
/**
 * 依赖函数，数据与节点的绑定依赖，采用"传统的观察者模式"
 */

var uid = 0;

function Dep() {
    this.id = uid++;
    this.subs = [];
}

Dep.prototype = {
	// 添加订阅
    addSub: function(sub) {
        this.subs.push(sub);
    },

	// 增加依赖
    depend: function() {
        Dep.target.addDep(this);
    },

	// 移除依赖
    removeSub: function(sub) {
        var index = this.subs.indexOf(sub);
        if (index != -1) {
            this.subs.splice(index, 1);
        }
    },

	// 通知所有订阅者
    notify: function(options) {
        this.subs.forEach(function(sub) {
            sub.update(options);
        });
    }
};

// 初始化为空，全局变量，用以缓存watcher对象，即相应回调函数
Dep.target = null;
