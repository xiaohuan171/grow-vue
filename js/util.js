var util = {
	/**
	 * 是否为对象(包括数组、正则等)
	 */
	isObject : function(obj) {
		var type = typeof obj;
		return type === 'function' || type === 'object' && !!obj;
	},
	
	/**
	 * 解析表达式
	 * with+eval会将表达式中的变量绑定到vm模型中，从而实现对变量的取值
	 * 被compiler和Watcher调用
	 */
	computeExpression: function(scope, exp) {
		try {
			with (scope) {
				return eval(exp);
			}
		} catch (e) {
			console.error('ERROR', e);
		}
	},
	
	/*
	 * 获取viewModel的值
	 */
	getVMVal: function(vm, exp) {
    	if(exp.indexOf(':') > 0) {
    		if(util.isObject(JSON.parse(exp))) {
    			var obj = JSON.parse(exp),
    				flag, key, value,
    				res = [];
    			for(key in obj) {
    				value = obj[key];
    				flag = util.computeExpression(vm, value);
    				if(!!flag) {
    					res.push(key);
    				}
    			}
    			return res.join(' ');
    		}
    	}
    	
    	var regCheck = /check/g;
    	if(regCheck.test(exp)) {
    		var res = [];
    		res.push(vm[exp]);
    		return res;
    	}
    	
        var val = vm._data;
        exp = exp.split('.');
        exp.forEach(function(k) {
            val = val[k];
        });
        return val;
    },
    
    /*
     * 设置viewModel的值
     */
    setVMVal: function(vm, exp, value) {
        var val = vm._data;
        exp = exp.split('.');
        exp.forEach(function(k, i) {
            // 非最后一个key，更新val的值
            if (i < exp.length - 1) {
                val = val[k];
            } else {
                val[k] = value;
            }
        });
    }
}
